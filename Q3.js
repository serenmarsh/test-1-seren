//Seren
//29/03
//10010681

//Declaring the variables
let id
let name
let unit
let bill
let total

id = Number(prompt("Please enter the customer's ID number:"))
name = prompt("Please enter the customer's name:"))
unit = Number(prompt(`Please enter the number of units that ${name} has used:`))

if (unit >= 600) {
    bill = "$2 per unit"
} 
else if (unit >= 400 && <600) {
    bill = "$1.80 per unit"
}
else if (unit >= 200 && <400) {
    bill = "$1.50 per unit"
}
else {
    bill = "$1.20 per unit"
}

total = Number(unit*bill).toFixed(2)

console.log("This application will calculate the electricity bill")
console.log("")
console.log(`ID : ${id}`)
console.log(`Name : ${name}`)
console.log(`Units : ${unit}`)
console.log("")
console.log(`The total owing @ ${bill} is: ${total}`)